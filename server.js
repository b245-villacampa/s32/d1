const http = require('http');
let port = 3000;

let directory = [
		{
			"name":"Brandon",
			"email":"brandon@email.com"
		},
		{
			"name":"Jobert",
			"email":"jobert@email.com"
		}

	]

http.createServer(function(request, response){
	// Add a route here for GET,method that will retrive tthe content of our database

	if(request.url === "/users" && request.method === "GET"){

		// sets the status to 200
		// application/json sets the response to JSON Data type
		response.writeHead(200, {"Content-Type":"application/json"});

		// Input has to be data type STRING hence the JSON.stringify(method)
		response.write(JSON.stringify(directory));

		// To end the route we add response.end
		response.end();
	}else if(request.url === "/users" && request.method ==="POST"){
		
		let requestBody = "";

		//to capture the input of the user and store the variable requestBody 
		request.on('data', function(data){
			requestBody+=data;
		})

		// to the end the request and set the function that will be invoked rioght before it ends
		request.on('end', function(){
			console.log(typeof requestBody);
			console.log(requestBody);
			requestBody = JSON.parse(requestBody);
			console.log(typeof requestBody);

			let newUser = {
				"name" : requestBody.name,
				"email" : requestBody.email
			}

			console.log(newUser);

			directory.push(newUser);

			response.writeHead(200, {'Content-Type':'application/json'});
			response.write(JSON.stringify(newUser));
			response.end();
		})
	}

}).listen(port);

console.log(`Server is running at port ${port}`);