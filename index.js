let http = require("http");

//create server to listen to a port 

http.createServer(function (request, response){

	if(request.url === "/items" && request.method === 'GET'){
		console.log()
		response.writeHead(200, {'Content-Type':'text/plain'});
		response.end("Data received from /items.");
	}

	if(request.url ==="/items" && request.method === 'POST'){
		response.writeHead(200,{'Content-Type':'text/plain'})
		response.end("Data to be sent to the database!");
	}
	

}).listen(4000);

// When server is running, console will print the message.
console.log('Server running at localhost:4000');